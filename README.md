## You can display colored text in ANSI code supported terminal or text viewer.  

![image](snapshot/doushi_mac.png)


By combining the ANSI code and the full-width space "　",  
it is possible to express the dot picture style of a retro video game.  

Now, you are ready to summon a DOSHI-TAN (FF3) (with a cute NEKO-MIMI) to your terminal.  

If you are a mac or linux user, you type cat or tail command.  
(You should use the cat command because NEKO-MIMI)  

```
$ cat doshi.tan
```

If you are a WIndows user, open a command prompt and type "type" command.  

```
> type doshi.tan
```
![image](snapshot/doushi_win10.png)


However, it does not seem to work with some Windows.  
(10 Pro worked, but 7 Home did not work.)  





---

## ANSIコードを使うと、対応したターミナルやテキストビューアーに色つきのテキストを表示することができます。  

全角スペース"　"とANSIコードを組み合わせると、あたかもビデオゲームのドット絵風の表現が可能です。  

さあ、ネコ耳がかわいい導師たん(FF3)をあなたのターミナルに召喚する準備が整いました。  

もしあなたがmacかlinuxユーザーなら、catやtailコマンドで導師たんを呼び出すことができます。  
（ネコ耳なのでcatコマンドを使うべきです）  

もしあなたがWIndowsユーザーなら、コマンドプロンプトを開いてtypeコマンドを入力してください。

ただし幾つかのWindowsでは動作しないようです。  
（10 Proは動作したが、7 Homeは動かなかった。）